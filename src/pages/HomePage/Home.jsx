import React, {useState} from 'react';
import './Home.scss';
import { Carousel } from 'primereact/carousel';
import '../../Shared/styles/blocks/primereact-carousel-block.scss';


export default function Home () {
    const [carousel] =  useState([
        {
            img:'https://assets.pokemon.com/assets/cms2/img/attend-events/championship/2020-team-challenge/2020-team-challenge-169.jpg',
            desc:'Tendremos nueva app de Pokemon este año',
        },
        {
            img:'https://i.ytimg.com/vi/CPJcaGWoO2c/maxresdefault.jpg',
            desc:'Super Mario 3D All Star',
        },
        {
            img:'https://www.cinemascomics.com/wp-content/uploads/2020/05/director-de-your-name-960x720.jpg',
            desc:'El director de Your Name mustra el avance de su nuevo proyecto',

        },
        
        {
            img:'https://hardzone.es/app/uploads-hardzone.es/2020/05/Dise%C3%B1o-PS5.jpg',
            desc:'Se filtra el precio de la PS5'
        },
        {
            img:'https://cde.peru.com//ima/0/1/4/6/2/1462627/924x530/disney.jpg',
            desc:'Los personajes de Disney estilo anime',
        }
    ]);

    const itemTemplate = (car) => {
        return <div>
            <img src={car.img} alt=""/>
            <p className="lines">{car.desc}</p>

        </div>
    };
    return (

        <div className="font carousel">
        
        <div className="c-backgroundImg">
        </div>
            <div className="c-title">
                <h2>Power up your gameplay!</h2>
                <p>Discover amiibo, a fun and unique way to interact with your favourite Nintendo characters and games!</p>
            </div>
            <Carousel className="b-primereact-carousel" value={carousel} itemTemplate={itemTemplate}></Carousel>
        </div>

        /* <ThemeProvider theme={lightTheme}> */
   
    )

}