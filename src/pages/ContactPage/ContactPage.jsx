import React from 'react';
import { useForm } from "react-hook-form";
import './ContactPage.scss';

export default function ContactPage() {

    const { register, handleSubmit, watch, errors } = useForm();
    const onSubmit = data => console.log(data);

    return (

        <form onSubmit={handleSubmit(onSubmit)} className="c-form">
        <h2 className="c-form__title">AMIIBOS FAVORITOS</h2>
            <label htmlFor="name">
                <span className="c-form__name">Name</span>
                <input className="c-form__inputName" name="name" id="name" ref={register({ required: true })} />
            </label>

            <label htmlFor="email">
                <span className="c-form__email">Email</span>

                <input className="c-form__inputEmail" name="email" id="email" defaultValue="amiibo@gmail.com" ref={register({ required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ })} />
            </label>

            <label htmlFor="favoriteAmiibo">
                <span className="c-form__favorite">Favorite amiibo</span>
                <select className="c-form__selectFavorite" name="favoriteAmiibo" id="favoriteAmiibo"
                    ref={register({ required: true })}>
                </select>
            </label>

            <label htmlFor="message">
                <span className="c-form__message">Message</span>
                <textarea className="c-form__textarea" name="message" id="message"
                    ref={register({ required: true, minLength: 5, maxLength: 255 })} />

            </label>

            <button className="c-form__button" type="submit" >Submit</button>

        </form>



    );

}