import React from 'react';
import './TextList.scss';


export default function TextList(props) {


    return (
        <div className="c-gameSeries-gallery">
           <div className= "row">
                {props.gameSeries.map((game, i) =>
                    <div className="col-12 col-md-6 col-lg-4" key={i}>
                        <div className="c-gameSeries-gallery__item-container">
                            <div className="c-gameSeries-gallery__item">
                            <h4 className="c-gameSeries-gallery__caption">{game.name}</h4>
                            </div>
                        </div>
                    </div>

                )}

            </div>
        </div>
    )
}