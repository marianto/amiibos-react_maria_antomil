import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import TextList from './TextList/TextList';

export default function GameSeriesPage() {


    const [localSeries, setLocalSeries] = useState([])
    useEffect(() => {
        axios.get(process.env.REACT_APP_BACK_URL + 'gameseries').then(res => {
        const filteredAmiibos = uniqueArray(res.data.amiibo, 'name');
        setLocalSeries(filteredAmiibos); 
        })
    }, [])

    return (
        <div>
            <h1 className="title justify-content-center">Game Series</h1>
            <TextList gameSeries={localSeries} />
        </div>
    );

}

//este array se utiliza para que en gameseries aparecen por defecto games con nombre repetido, de esta manera lo que hace es que si el nombre esta repetido no lo incluya en la galeria//

const uniqueArray = (array, key) => {
    return array.filter((item, index, self) =>
        index === self.findIndex((t) => (
            t[key] === item[key]
        ))
    )
};