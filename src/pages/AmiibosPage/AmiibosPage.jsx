import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import AmiibosGallery from './Components/AmiibosGallery';
import AmiibosSearch from './Components/AmiiboSearch/AmiiboSearch';

let amiibos = [];

export default function AmiibosPage() {

    const [localAmiibos, setLocalAmiibos] = useState([])
    const [filteredAmiibos, setFilteredAmiibos] = useState([]);

    useEffect(() => {

       
       
            axios.get(process.env.REACT_APP_BACK_URL +'amiibo'
            ).then(res => {
                const amiibosLocal = res.data.amiibo;
                console.log(res);
                amiibos = amiibosLocal;
                setFilteredAmiibos(amiibosLocal);
            })

    

    }, [])

    

    const filterAmiibos = (filterValues) => {
        console.log(filterValues);
        const filteredLocalAmiibos = [];

        for (const amiibo of amiibos) {
            if (amiibo.name.includes(filterValues.name)) {
                filteredLocalAmiibos.push(amiibo);
            }
        }

        setFilteredAmiibos(filteredLocalAmiibos);

    }



    return (

        <div>
            
            <AmiibosSearch fnSubmit={filterAmiibos} />
            <AmiibosGallery amiibos={filteredAmiibos} />
        </div>
    );

}