import React from 'react';
import './AmiiboSearch.scss'
import { useForm } from "react-hook-form";


export default function AmiiboSearch (props) {

    const { register, handleSubmit, watch, errors } = useForm();


    const fnChange = (data) => {
        props.fnSubmit(data)
    };

    return (
        <form className="search-form" onSubmit={handleSubmit(fnChange)}>
            <label htmlFor="name">
                <span className="b-text-label"></span>
                <input className="search-form_input" placeholder="Search..." onChange={handleSubmit(fnChange)} name="name" id="name"
                       ref={register}/>
            </label>
        </form>
    )
}