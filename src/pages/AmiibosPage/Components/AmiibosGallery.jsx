import React from 'react';
import { Link } from 'react-router-dom';
import './AmiibosGallery.scss';



export default function AmiibosGallery(props) {


    return (
        <div className="c-amiibo-gallery">
            <div className="row">
                {props.amiibos.map((amiibo, i) =>
                    <div className="col-12 col-md-6 col-lg-4" key={i}>
                       
                            <div className="c-amiibo-gallery__item-container">
                                <figure className="c-amiibo-gallery__item">
                                <Link to={"amiibos/" + amiibo.tail}>
                                    <img className="c-amiibo-gallery__img" src={amiibo.image}></img>
                                    <figcaption className="c-amiibo-gallery__caption">{amiibo.name}</figcaption>
                                </Link>
                                </figure>
                            </div>
                      
                    </div>

                )}

            </div>
        </div>
    )
}