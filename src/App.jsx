import React from 'react';
import './App.css';
import AmiibosPage from './pages/AmiibosPage/AmiibosPage';
import Home from './pages/HomePage/Home';
import AmiibosGallery from './pages/AmiibosPage/Components/AmiibosGallery';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";
import GameSeriesPage from './pages/GameSeriesPage/GameSeriesPage';
import TextList from './pages/GameSeriesPage/TextList/TextList';
import ContactPage from './pages/ContactPage/ContactPage';
import AmiibosDetailPage from './pages/AmiibosPage/pages/AmiibosDetailPage/AmiibosDetailPage';
import Menu from './Shared/menu/Menu';

function App() {
    return (
        <Router>
            <div className="container-fluid justify-content-center my-4 u-font-size-16">
               <Menu/>

                    <Switch>
                        <Route path="/amiibos/:tail">
                            <AmiibosDetailPage />
                        </Route>
                        <Route path="/amiibos">
                            <AmiibosPage />
                        </Route>
                        <Route path="/gameseries">
                            <GameSeriesPage/>
                        </Route>
                        <Route path="/contact">
                            <ContactPage/>
                        </Route>
                        <Route path="/">
                            <Home/>
                        </Route>
                    </Switch>
                
            </div>

        </Router>

    );
}

export default App;
